import { Component } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-smart-cities-dashboard',
  templateUrl: './smart-cities-dashboard.component.html',
  styleUrls: ['./smart-cities-dashboard.component.css']
})
export class SmartCitiesDashboardComponent {
  cityName: string;
  // google maps zoom level
  zoom: number = 9;

  // initial center position for the map
  lat: number = 11.0168;
  lng: number = 76.9558;
  latMadurai: number = 9.9252;
  lngMadurai: number = 78.1198;
  latSalem: number = 11.6643;
  lngSalem: number = 78.1460;
  latThanjavur: number = 10.7870;
  lngThanjavur: number = 79.1378;
  latTiruchirapalli: number = 10.7905;
  lngTiruchirapalli: number = 78.7047;
  latVellore: number = 12.9165;
  lngVellore: number = 79.1325;
  latTirunelveli: number = 8.7139;
  lngTirunelveli: number = 77.7567;
  latTiruppur: number = 11.1085;
  lngTiruppur: number = 77.3411;
  latThoothukudi: number = 8.7642;
  lngThoothukudi: number = 78.1348;
  latErode: number = 11.3410;
  lngErode: number = 77.7172;
  coimbatore: string = 'Coimbatore';
  madurai:string = 'Madurai';
  salem:string = 'Salem';
  thanjavur:string = 'Thanjavur';
  tiruchirapalli:string = 'Tiruchirapalli';
  vellore:string = 'Vellore';
  tirunelveli:string = 'Tirunelveli';
  tiruppur:string = 'Tiruppur';
  thoothukudi:string = 'Thoothukudi';
  erode:string = 'Erode';
  constructor(private router: Router) {

  }
  click(x) {
    // alert(x);
    this.cityName = x;
    this.router.navigate(['weather/' + this.cityName]);
  }
  clickedMarker(label: any, index: number) {
    console.log(`clicked the marker: ${label || index}`);
    console.log('city   ', label);
    this.cityName = label;
    this.router.navigate(['weather/' + this.cityName]);
  }

  mapClicked($event: MouseEvent) {
  //   console.log('eventssssssssssss ', $event);
  //   this.markers.push({
  //     lat: $event.coords.lat,
  //     lng: $event.coords.lng,
  //     draggable: true,
  //     city: ''
  //   });
  }
  
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  // tslint:disable-next-line:member-ordering
  markers: marker[] = [
    {
		  lat: 11.0168,
		  lng: 76.9558,
		  label: 'C',
      draggable: true,
      city: 'Coimbatore'
	  },
  ];
  markersMadurai: marker[] = [
    {
		  lat: 9.9252,
		  lng: 78.1198,
		  label: 'M',
      draggable: true,
      city: 'Madurai'
	  },
  ];
  markersSalem: marker[] = [
    {
		  lat: 11.6643,
		  lng: 78.1460,
		  label: 'S',
      draggable: true,
      city: 'Salem'
	  },
  ];
  markersThanjavur: marker[] = [
    {
		  lat: 10.7870,
		  lng: 79.1378,
		  label: 'T',
      draggable: true,
      city: 'Thanjavur'
	  },
  ];
  markersTiruchirapalli: marker[] = [
    {
		  lat: 10.7905,
		  lng: 78.7047,
		  label: 'T',
      draggable: true,
      city: 'Tiruchirapalli'
	  },
  ];
  markersVellore: marker[] = [
    {
		  lat: 12.9165,
		  lng: 79.1325,
		  label: 'V',
      draggable: true,
      city: 'Vellore'
	  },
  ];
  markersTirunelveli: marker[] = [
    {
		  lat: 8.7139,
		  lng: 77.7567,
		  label: 'T',
      draggable: true,
      city: 'Tirunelveli'
	  },
  ];
  markersTiruppur: marker[] = [
    {
		  lat: 11.1085,
		  lng: 77.3411,
		  label: 'T',
      draggable: true,
      city: 'Tiruppur'
	  },
  ];
  markersThoothukudi: marker[] = [
    {
		  lat: 8.7642,
		  lng: 78.1348,
		  label: 'T',
      draggable: true,
      city: 'Thoothukudi'
	  },
  ];
  markersErode: marker[] = [
    {
		  lat: 11.3410,
		  lng: 77.7172,
		  label: 'E',
      draggable: true,
      city : 'Erode'
	  },
  ];
}
// just an interface for type safety.
// tslint:disable-next-line:class-name
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  city: string;
}
