import { Component, OnInit } from '@angular/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
declare var $: any;
@Component({
  selector: 'app-pressure',
  templateUrl: './pressure.component.html',
  styleUrls: ['./pressure.component.css'],
  providers: [GraphsService]
})
export class PressureComponent implements OnInit {

  airPreData: any;
  messageSuccess: boolean;
  single: any[];
  single1: any[];
  multi: any[];
  colorScheme1: any;
  view: any[] = [200, 250];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = 'Pascals';
  showGridLines = true;
  animations = false;
  yScaleMax = 1050;
  activeEntries = {};
  colorScheme = {
    domain: ['#9fff80']
  };
  constructor(private GS: GraphsService) {
    // Object.assign(this, { single });
    setInterval(() => { this.getStreaming(); }, 1000 * 6 * 1);
  }
  onSelect(event) {
    // console.log('eventttt', single);
    // if (event.value === 100) {
    //   this.colorScheme = {
    //     domain: ['#C7B42C', '#A10A28', '#C7B42C', '#AAAAAA']
    //   };
    // }
    // var that = this; // no need of this line
    this.messageSuccess = true;

    // setTimeout(() => {
    //   this.multiCall();
    //   this.constructor();
    // }, 6000);
  }
  // tslint:disable-next-line:member-ordering
  customColors = [
    {
      name: 'Pressure',
      value: ['#A10A28']
    },
    {
      name: 'Pressure',
      value: ['#C7B42C']
    }
  ];

  ngOnInit() {
    // this.GS.getAirData().subscribe(data => {
    //   for (let i = 0; i < data.length; i++) {
    //     // console.log('000000000 ', data);
    //     const dataAir = {
    //       name: Number('Pressure'),
    //       value: Number(data[i].airPressure)
    //     };
    //     this.airPreData.push(dataAir);
    //   }
    //   // console.log('airrrrrrrrrrrrrrrrrrrr ', this.airPreData);
    // });
}
getStreaming() {
  this.GS.getPressure().subscribe(data => {
    // console.log('pressure000000000000000000', data);
    if (data[0].value > 1020) {
      // alert('Air pressure got increased' + data[0].value);
      this.colorScheme1 = {
        domain: ['#A10A28']
      };
      this.colorScheme = this.colorScheme1;
      // $('#pressurePopUp').modal('show');
    } else {
      this.colorScheme = {
        domain: ['#9fff80']
      };
    }
    this.single = data;
  });
}

}
