import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.css'],
  providers: [GraphsService]
})
export class PortalComponent implements OnInit {
name: any = '';
phone: any = '';
msg: any = '';
portalDynamicForm: FormGroup
  constructor(private route: ActivatedRoute, private router: Router, private GS: GraphsService) { }

  ngOnInit() {
    this.portalDynamicForm = new FormGroup({
      'phoneNumber': new FormArray([])
    });
    const control = new FormControl('', Validators.required);
    (<FormArray>this.portalDynamicForm.get('phoneNumber')).push(control);
  }
  // splitBill(): void {
  //   this.portalDynamicForm = new FormGroup({
  //     'phoneNumber': new FormArray([])
  //   });
  //   const control = new FormControl('', Validators.required);
  //   (<FormArray>this.portalDynamicForm.get('phoneNumber')).push(control);
  // }

  addMorePeople(index) {
    const control = new FormControl('', Validators.required);
       (<FormArray>this.portalDynamicForm.get('phoneNumber')).push(control);
  }

  removeSplitBill(index) {
    const control = <FormArray>this.portalDynamicForm.controls['phoneNumber'];
    control.removeAt(index);
  }

  submit(x) {
    // alert(JSON.stringify(x));
    // console.log(this.name, this.phone, this.msg);
    let obj = {
      "message": this.msg,
      "dest":x
      }
    this.GS.sendSMS(obj).subscribe(data => {
    //   console.log('dataaaaaaaa ', data);
    //   this.name = '';
    //   this.phone = '';
    this.portalDynamicForm= new FormGroup({
      'phoneNumber': new FormArray([])
    });
     
    });
    this.msg = '';
  }
}
