import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  providers: [GraphsService]
})
export class MapComponent implements OnInit {
  // ngOnInit() {
  // }
  // google maps zoom level
  zoom: number = 8;

  // initial center position for the map
  lat: number = 13.0822;
  lng: number = 80.2755;



  markers = [];
  constructor(private GS: GraphsService, private router: Router) { }
  mouseOver(event) {
    console.log(event);
  }
  ngOnInit() {
    this.GS.getMapData().subscribe(data => {
      console.log('56' + JSON.stringify(data));
      // debugger;
      // for (let i = 0; i < data.length; i++) {
      //   //   data[i].latitude = Number(data[i].latitude);
      //    if (data[i].city === 'Tiruchirappalli') {
      //     data[i].iconUrl = 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png';
      //   } else if (data[i].city === 'Madurai') {
      //     data[i].iconUrl = 'https://maps.google.com/mapfiles/kml/shapes/info-i_maps.png';
      //   }
      //   this.markers.push(data[i]);
      // }
       this.markers = data;
    });
  }
  mouseClick(event) {
    console.log('markerClickmarkerClick ', event);
  }
  // zoom: number = 8;

  // // initial center position for the map
  // lat: number = 51.673858;
  // lng: number = 7.815982;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
    console.log();
    this.router.navigate(['city']);
  }

  // mapClicked($event: MouseEvent) {
  //   this.markers.push({
  //     lat: $event.coords.lat,
  //     lng: $event.coords.lng,
  //     draggable: true
  //   });
  // }

  // markerDragEnd(m: marker, $event: MouseEvent) {
  //   console.log('dragEnd', m, $event);
  // }

  // markers: marker[] = [
  //   {
  //     lat: 51.673858,
  //     lng: 7.815982,
  //     label: 'A',
  //     draggable: true
  //   },
  //   {
  //     lat: 51.373858,
  //     lng: 7.215982,
  //     label: 'B',
  //     draggable: false
  //   },
  //   {
  //     lat: 51.723858,
  //     lng: 7.895982,
  //     label: 'C',
  //     draggable: true
  //   }
  // ]


}
// interface marker {
//   lat: number;
//   lng: number;
//   label?: string;
//   draggable: boolean;
// }