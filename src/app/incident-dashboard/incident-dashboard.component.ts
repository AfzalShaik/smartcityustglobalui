import { Component, OnInit } from '@angular/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-incident-dashboard',
  templateUrl: './incident-dashboard.component.html',
  styleUrls: ['./incident-dashboard.component.css'],
  providers : [GraphsService]
})
export class IncidentDashboardComponent implements OnInit {

  name: any;
  parking1: any;
  parking2: any;
  parking3: any;
  mass1: any;
  mass2: any;
  mass3: any;
  natural3: any;
  natural2: any;
  natural1: any;
  fire1: any;
  fire2: any;
  fire3: any;
  accident1: any;
  accident2: any;
  accident3: any;
  road1: any;
  road2: any;
  road3: any;
  areaName: any;
  paramData: any;
  parkingArray: any;
  massArray: any;
  naturalArray: any;
  fireArray: any;
  accidentArray: any;
  roadArray1: any;
  roadArray: any;
  cityArray: string[];
  constructor(private GS: GraphsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.paramData = this.route.params.subscribe(params => {
      this.areaName = params.id;
      // alert('.. '+ params.id);
      this.areaName = (params.id) ? params.id : 'Coimbatore';
      this.pressureChanged(this.areaName);
    });
    // this.name = this.areaName;
    this.cityArray = ['Coimbatore', 'Madurai', 'Salem', 'Thanjavur', 'Tiruchirapalli', 'Vellore', 'Tirunelveli', 'Tiruppur', 'Thoothukudi', 'Erode'];
   
  }
  abcd(x){
    this.areaName = (x) ? x + '@$' + this.areaName : this.areaName;
    this.router.navigate(['natural/' + this.areaName]);
    // alert('.. '+ JSON.stringify(x));
  }
  pressureChanged(x) {
    console.log('xxxxxxxxxxx ', x);
    // this.roadArray = [];
    // this.accidentArray = [];
    // this.fireArray = [];
    // this.naturalArray = [];
    // this.massArray = [];
    // this.parkingArray = [];
    this.name = x;
    this.GS.getIncidentRoad(this.name).subscribe(data => {
      this.roadArray = data;
      // alert(JSON.stringify(this.roadArray));
      this.road1 = this.roadArray[0].news;
      this.road2 = this.roadArray[1].news;
      this.road3 = this.roadArray[2].news;
    });
    this.GS.getIncidentAccident(this.name).subscribe(data => {
      this.accidentArray = data;
      this.accident1 = this.accidentArray[0].news;
      this.accident2 = this.accidentArray[1].news;
      this.accident3 = this.accidentArray[2].news;
    });
    this.GS.getIncidentFire(this.name).subscribe(data => {
      this.fireArray = data;
      this.fire1 = this.fireArray[0].news;
      this.fire2 = this.fireArray[1].news;
      // this.fire3 = this.fireArray[2].news;
    });
    this.GS.getIncidentNatural(this.name).subscribe(data => {
      this.naturalArray = data;
      this.natural1 = this.naturalArray[0].news;
      this.natural2 = this.naturalArray[1].news;
      this.natural3 = this.naturalArray[2].news;
    });
    this.GS.getIncidentMass(this.name).subscribe(data => {
      this.massArray = data;
      this.mass1 = this.massArray[0].news;
      this.mass2 = this.massArray[1].news;
      this.mass3 = this.massArray[2].news;
    });
    this.GS.getIncidentParking(this.name).subscribe(data => {
      this.parkingArray = data;
      this.parking1 = this.parkingArray[0].news;
      this.parking2 = this.parkingArray[1].news;
      this.parking3 = this.parkingArray[2].news;
    });
  //   if (x === 'Coimbatore') {
  //     this.GS.getIncidentRoad('Coimbatore').subscribe(data => {
  //       this.roadArray = data;
  //     });
  //     this.GS.getIncidentAccident('Coimbatore').subscribe(data => {
  //       this.accidentArray = data;
  //     });
  //     this.GS.getIncidentFire('Coimbatore').subscribe(data => {
  //       this.fireArray = data;
  //     });
  //     this.GS.getIncidentNatural('Coimbatore').subscribe(data => {
  //       this.naturalArray = data;
  //     });
  //     this.GS.getIncidentMass('Coimbatore').subscribe(data => {
  //       this.massArray = data;
  //     });
  //     this.GS.getIncidentParking('Coimbatore').subscribe(data => {
  //       this.parkingArray = data;
  //     });
  //   }
  //  else if (x === 'Madurai') {
  //   this.roadArray = [];
  //   this.accidentArray = [];
  //   this.fireArray = [];
  //   this.naturalArray = [];
  //   this.massArray = [];
  //   this.parkingArray = [];
  //     this.GS.getIncidentRoad('Madurai').subscribe(data => {
  //       this.roadArray = data;
  //     });
  //     this.GS.getIncidentAccident('Madurai').subscribe(data => {
  //       this.accidentArray = data;
  //     });
  //     this.GS.getIncidentFire('Madurai').subscribe(data => {
  //       this.fireArray = data;
  //     });
  //     this.GS.getIncidentNatural('Madurai').subscribe(data => {
  //       this.naturalArray = data;
  //     });
  //     this.GS.getIncidentMass('Madurai').subscribe(data => {
  //       this.massArray = data;
  //     });
  //     this.GS.getIncidentParking('Madurai').subscribe(data => {
  //       this.parkingArray = data;
  //     });
  //   }
  //   else if (x === 'Salem') {
  //     this.roadArray = [];
  //     this.accidentArray = [];
  //     this.fireArray = [];
  //     this.naturalArray = [];
  //     this.massArray = [];
  //     this.parkingArray = [];
  //     this.GS.getIncidentRoad('Salem').subscribe(data => {
  //       this.roadArray = data;
  //     });
  //     this.GS.getIncidentAccident('Salem').subscribe(data => {
  //       this.accidentArray = data;
  //     });
  //     this.GS.getIncidentFire('Salem').subscribe(data => {
  //       this.fireArray = data;
  //     });
  //     this.GS.getIncidentNatural('Salem').subscribe(data => {
  //       this.naturalArray = data;
  //     });
  //     this.GS.getIncidentMass('Salem').subscribe(data => {
  //       this.massArray = data;
  //     });
  //     this.GS.getIncidentParking('Salem').subscribe(data => {
  //       this.parkingArray = data;
  //     });
  //   }
  //   else if (x === 'Thanjavur') {
  //     this.roadArray = [];
  //     this.accidentArray = [];
  //     this.fireArray = [];
  //     this.naturalArray = [];
  //     this.massArray = [];
  //     this.parkingArray = [];
  //     this.GS.getIncidentRoad('Thanjavur').subscribe(data => {
  //       this.roadArray = data;
  //     });
  //     this.GS.getIncidentAccident('Thanjavur').subscribe(data => {
  //       this.accidentArray = data;
  //     });
  //     this.GS.getIncidentFire('Thanjavur').subscribe(data => {
  //       this.fireArray = data;
  //     });
  //     this.GS.getIncidentNatural('Thanjavur').subscribe(data => {
  //       this.naturalArray = data;
  //     });
  //     this.GS.getIncidentMass('Thanjavur').subscribe(data => {
  //       this.massArray = data;
  //     });
  //     this.GS.getIncidentParking('Thanjavur').subscribe(data => {
  //       this.parkingArray = data;
  //     });
  //   }
  //   else if (x === 'Tiruchirapalli') {
  //     this.roadArray = [];
  //     this.accidentArray = [];
  //     this.fireArray = [];
  //     this.naturalArray = [];
  //     this.massArray = [];
  //     this.parkingArray = [];
  //     this.GS.getIncidentRoad('Tiruchirapalli').subscribe(data => {
  //       this.roadArray = data;
  //     });
  //     this.GS.getIncidentAccident('Tiruchirapalli').subscribe(data => {
  //       this.accidentArray = data;
  //     });
  //     this.GS.getIncidentFire('Tiruchirapalli').subscribe(data => {
  //       this.fireArray = data;
  //     });
  //     this.GS.getIncidentNatural('Tiruchirapalli').subscribe(data => {
  //       this.naturalArray = data;
  //     });
  //     this.GS.getIncidentMass('Tiruchirapalli').subscribe(data => {
  //       this.massArray = data;
  //     });
  //     this.GS.getIncidentParking('Tiruchirapalli').subscribe(data => {
  //       this.parkingArray = data;
  //     });
  //   }
  //   else if (x === 'Vellore') {
  //     this.roadArray = [];
  //     this.accidentArray = [];
  //     this.fireArray = [];
  //     this.naturalArray = [];
  //     this.massArray = [];
  //     this.parkingArray = [];
  //     this.GS.getIncidentRoad('Vellore').subscribe(data => {
  //       this.roadArray = data;
  //     });
  //     this.GS.getIncidentAccident('Vellore').subscribe(data => {
  //       this.accidentArray = data;
  //     });
  //     this.GS.getIncidentFire('Vellore').subscribe(data => {
  //       this.fireArray = data;
  //     });
  //     this.GS.getIncidentNatural('Vellore').subscribe(data => {
  //       this.naturalArray = data;
  //     });
  //     this.GS.getIncidentMass('Vellore').subscribe(data => {
  //       this.massArray = data;
  //     });
  //     this.GS.getIncidentParking('Vellore').subscribe(data => {
  //       this.parkingArray = data;
  //     });
  //   }
  //   else if (x === 'Tirunelveli') {
  //     this.roadArray = [];
  //     this.accidentArray = [];
  //     this.fireArray = [];
  //     this.naturalArray = [];
  //     this.massArray = [];
  //     this.parkingArray = [];
  //     this.GS.getIncidentRoad('Tirunelveli').subscribe(data => {
  //       this.roadArray = data;
  //     });
  //     this.GS.getIncidentAccident('Tirunelveli').subscribe(data => {
  //       this.accidentArray = data;
  //     });
  //     this.GS.getIncidentFire('Tirunelveli').subscribe(data => {
  //       this.fireArray = data;
  //     });
  //     this.GS.getIncidentNatural('Tirunelveli').subscribe(data => {
  //       this.naturalArray = data;
  //     });
  //     this.GS.getIncidentMass('Tirunelveli').subscribe(data => {
  //       this.massArray = data;
  //     });
  //     this.GS.getIncidentParking('Tirunelveli').subscribe(data => {
  //       this.parkingArray = data;
  //     });
  //   }
  //   else if (x === 'Tiruppur') {
  //     this.roadArray = [];
  //     this.accidentArray = [];
  //     this.fireArray = [];
  //     this.naturalArray = [];
  //     this.massArray = [];
  //     this.parkingArray = [];
  //     this.GS.getIncidentRoad('Tiruppur').subscribe(data => {
  //       this.roadArray = data;
  //     });
  //     this.GS.getIncidentAccident('Tiruppur').subscribe(data => {
  //       this.accidentArray = data;
  //     });
  //     this.GS.getIncidentFire('Tiruppur').subscribe(data => {
  //       this.fireArray = data;
  //     });
  //     this.GS.getIncidentNatural('Tiruppur').subscribe(data => {
  //       this.naturalArray = data;
  //     });
  //     this.GS.getIncidentMass('Tiruppur').subscribe(data => {
  //       this.massArray = data;
  //     });
  //     this.GS.getIncidentParking('Tiruppur').subscribe(data => {
  //       this.parkingArray = data;
  //     });
  //   }
  //   else if (x === 'Thoothukudi') {
  //     this.roadArray = [];
  //     this.accidentArray = [];
  //     this.fireArray = [];
  //     this.naturalArray = [];
  //     this.massArray = [];
  //     this.parkingArray = [];
  //     this.GS.getIncidentRoad('Thoothukudi').subscribe(data => {
  //       this.roadArray = data;
  //     });
  //     this.GS.getIncidentAccident('Thoothukudi').subscribe(data => {
  //       this.accidentArray = data;
  //     });
  //     this.GS.getIncidentFire('Thoothukudi').subscribe(data => {
  //       this.fireArray = data;
  //     });
  //     this.GS.getIncidentNatural('Thoothukudi').subscribe(data => {
  //       this.naturalArray = data;
  //     });
  //     this.GS.getIncidentMass('Thoothukudi').subscribe(data => {
  //       this.massArray = data;
  //     });
  //     this.GS.getIncidentParking('Thoothukudi').subscribe(data => {
  //       this.parkingArray = data;
  //     });
  //   }
  //   else if (x === 'Erode') {
  //     this.roadArray = [];
  //     this.accidentArray = [];
  //     this.fireArray = [];
  //     this.naturalArray = [];
  //     this.massArray = [];
  //     this.parkingArray = [];
  //     this.GS.getIncidentRoad('Erode').subscribe(data => {
  //       this.roadArray = data;
  //     });
  //     this.GS.getIncidentAccident('Erode').subscribe(data => {
  //       this.accidentArray = data;
  //     });
  //     this.GS.getIncidentFire('Erode').subscribe(data => {
  //       this.fireArray = data;
  //     });
  //     this.GS.getIncidentNatural('Erode').subscribe(data => {
  //       this.naturalArray = data;
  //     });
  //     this.GS.getIncidentMass('Erode').subscribe(data => {
  //       this.massArray = data;
  //     });
  //     this.GS.getIncidentParking('Erode').subscribe(data => {
  //       this.parkingArray = data;
  //     });
  //   }
  }
  bridge1(x) {
    console.log('hellooooo ', x);
  }
}
