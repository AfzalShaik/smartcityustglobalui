import { Component, OnInit, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MouseEvent } from '@agm/core';
import { Router } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { ActivatedRoute } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-weather-dashboard',
  templateUrl: './weather-dashboard.component.html',
  styleUrls: ['./weather-dashboard.component.css'],
  providers : [GraphsService]
})
export class WeatherDashboardComponent implements OnInit {
  textAlert: string;
  selectedValue: any;
  paramData: any;
  maxValue: number;
  arr: any;
  areaName: string;
  waterLevel: number;
  erode: any;
  thoothkudiValue: number;
  tiruppurValue: number;
  tirunelveliValue: number;
  velloreValue: number;
  tiruchirapalli: number;
  thanjavurValue: number;
  salemValue: number;
  salem: number;
  maduraiValue: number;
  coimbatoreValue: number;
  randomNumber: number;
  obj: {};
  zoom: number = 9;
  single: any[];
  i:any;
  // initial center position for the map
  lat: number = 11.0168;
  lng: number = 76.9558;

  future2: any;
  future1: any;
  tomorrowWind: any;
  todayWind: any;
  yesterdayWind: any;
  futureWeather1: any;
  futureWeather2: any;
  tomorrowWeather: any;
  todayWeather: any;
  yesterdayWeather: any;
  city : any;
  cityArray : any;
  currentDate: any;
  future1date: any;
  future2date: any;
  futureWater1: any;
  futureWater2: any;
  tomorrowWater: any;
  todayWater: any;
  yesterdayWater: any;
  news1: any;
  news2: any;
  news3: any;
  news4: any;
  news5: any;
  news6: any;
  news7: any;
  news8: any;
  view: any[] = [300, 300];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  constructor(private router: Router, private GS: GraphsService, private route: ActivatedRoute) {
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
  });
    // Object.assign(this, { single })
    // setInterval(() => { this.getStreaming(); }, 1000 * 60 * 1);
    // console.log('000000000000000000000');
   }

  ngOnInit() {
    // alert(document.getElementById('incident').id);
    // document.getElementById('incident').id
    
    this.selectedValue = 'Coimbatore';
    this.paramData = this.route.params.subscribe(params => {
      // alert('ddddddddddddddd '+ params.id);
       this.areaName = params.id;
       this.city = params.id;
       this.selectedValue = this.city;
       if (this.city === 'Thanjavur') {
         this.textAlert = 'Water Levels Increased In ' + this.city
       } else if (this.city === 'Tirunelveli') {
        this.textAlert = 'Water Levels Increased In ' + this.city
       } else if (this.city === 'Thoothukudi') {
        this.textAlert = 'Water Levels Increased In ' + this.city
       } else if (this.city === 'Coimbatore') { 
        this.textAlert = 'Major Fire Breakes Out In Ramnathapuram.'
      } else {
        this.textAlert = 'Major Fire Breakes Out In ' + this.city
       }
       this.pressureChanged(this.city);
       });
      
    this.maxValue = Math.floor(Math.random() * 20) + 1;
    // this.areaName = 'Coimbatore';
    $('#waterlevelPopUp').modal('hide');
    this.currentDate = new Date();
    // console.log('dateeeeeeeeee ', this.currentDate.getDate());
    this.future1date = this.currentDate.getDate() + 2;
    this.future2date = this.currentDate.getDate() + 3;
    // air pressure
    // this.yesterdayWind = '6kmph';
    // this.todayWind = '7kmph';
    // this.tomorrowWind = '5kmph';
    // this.future1 = '8kmph';
    // this.future2 = '6kmph';
    // weather
    // this.yesterdayWeather = '6kmph';
    // this.todayWeather = '7kmph';
    // this.tomorrowWeather = '5kmph';
    // this.futureWeather2 = '8kmph';
    // this.futureWeather1 = '6kmph';
    // water level
    // this.futureWater1 = '2500Units';
    // this.futureWater2 = '2600Units';
    // this.tomorrowWater = '2400Units';
    // this.todayWater = '2800Units';
    // this.yesterdayWater = '2200Units';
    // this.city = 'Coimbatore';
    this.cityArray = ['Coimbatore', 'Madurai', 'Salem', 'Thanjavur', 'Tiruchirapalli', 'Vellore', 'Tirunelveli', 'Tiruppur', 'Thoothukudi', 'Erode']
    // this.news1 = 'Coimbatore Corporation is all set to develop four parks under the Smart Cities mission.';
    // this.news2 = 'Elephants rejuvenation camp in Mettupalayam.';
    // this.news3 = 'Petrol Bomb Hurled at CPI(M) Office Coimbatore, No One Injured.';
    // this.news4 = 'A record sighting of Frosted Duskywing butterfly.';
    // this.news5 = 'Petrol Bombs Hurled at BJP Office in Coimbatore Hours After Vandals Damaged Periyar Statue.';
    // this.news6 = 'Protests continue over Cauvery Board in Coimbatore.';
    // this.news7 = 'BJP Office Attack: 3 TPDK Members Detained Under Goondas Act.';
    // this.news8 = '4 Dead, Many Trapped as Bus Stand Roof Collapses in Coimbatore.';
    // water
    this.randomNumber = Math.floor(Math.random() * (2.5 - 1)) + 1;
    this.coimbatoreValue = 18.02 + this.randomNumber;
    this.maduraiValue = 9.41 + this.randomNumber;
    this.salemValue = 13.94 + this.randomNumber;
    this.thanjavurValue = 4.19 + this.randomNumber;
    this.tiruchirapalli = 9.89 + this.randomNumber;
    this.velloreValue = 10.26 + this.randomNumber;
    this.tirunelveliValue = 10.69 + this.randomNumber;
    this.tiruppurValue = 15.16 + this.randomNumber;
    this.thoothkudiValue = 8.80 + this.randomNumber;
    this.erode = 13.17 + this.randomNumber;
    this.single = [
      {
        "name": "Coimbatore",
        "value": this.coimbatoreValue
      },
      {
        "name": "Madurai",
        "value": this.maduraiValue
      },
      {
        "name": "Salem",
        "value": this.salemValue
      },
      {
        "name": "Thanjavur",
        "value": this.thanjavurValue
      },
      {
        "name": "Tiruchirapalli",
        "value": this.tiruchirapalli
      },
      {
        "name": "Vellore",
        "value": this.velloreValue
      },
      {
        "name": "Tirunelveli",
        "value": this.tirunelveliValue
      },
      {
        "name": "Tiruppur",
        "value": this.tiruppurValue
      },
      {
        "name": "Thoothukudi",
        "value": this.thoothkudiValue
      },
      {
        "name": "Erode",
        "value": this.erode
      }
    ];
  }
  getStreaming() {
    // console.log('calleddddddd');
    this.randomNumber = Math.floor(Math.random() * (2.5 - 1)) + 1;
    this.coimbatoreValue = 18.02 + this.randomNumber;
    this.maduraiValue = 9.41 + this.randomNumber;
    this.salemValue = 13.94 + this.randomNumber;
    this.thanjavurValue = 4.19 + this.randomNumber;
    this.tiruchirapalli = 9.89 + this.randomNumber;
    this.velloreValue = 10.26 + this.randomNumber;
    this.tirunelveliValue = 10.69 + this.randomNumber;
    this.tiruppurValue = 15.16 + this.randomNumber;
    this.thoothkudiValue = 8.80 + this.randomNumber;
    this.erode = 13.17 + this.randomNumber;
    // this.arr.push(this.coimbatoreValue, this.maduraiValue, this.salemValue, this.thanjavurValue, this.tiruchirapalli, this.velloreValue, this.tirunelveliValue, this.tiruppurValue, this.thoothkudiValue, this.erode);
    // this.maxValue = Math.max(this.coimbatoreValue, this.maduraiValue, this.salemValue, this.thanjavurValue, this.tiruchirapalli, this.velloreValue, this.tirunelveliValue, this.tiruppurValue, this.thoothkudiValue, this.erode);
    // console.log('maxxxxxxxxxxxxxxxxxxx  ', this.maxValue);
    this.maxValue = Math.floor(Math.random() * 20) + 1
    // console.log('maxxxxxxxxxxxxxxxxxxx  ', this.maxValue);
    this.single = [
      {
        "name": "Coimbatore",
        "value": this.coimbatoreValue
      },
      {
        "name": "Madurai",
        "value": this.maduraiValue
      },
      {
        "name": "Salem",
        "value": this.salemValue
      },
      {
        "name": "Thanjavur",
        "value": this.thanjavurValue
      },
      {
        "name": "Tiruchirapalli",
        "value": this.tiruchirapalli
      },
      {
        "name": "Vellore",
        "value": this.velloreValue
      },
      {
        "name": "Tirunelveli",
        "value": this.tirunelveliValue
      },
      {
        "name": "Tiruppur",
        "value": this.tiruppurValue
      },
      {
        "name": "Thoothukudi",
        "value": this.thoothkudiValue
      },
      {
        "name": "Erode",
        "value": this.erode
      }
    ];
    if (this.maxValue > 10 && this.maxValue < 20) {
      this.areaName = this.areaName;
      $('#waterlevelPopUp').modal('show');
    } 
    // console.log('coim, madh, sale         ', this.thanjavurValue, this.tiruchirapalli, this.velloreValue);
    // if(this.coimbatoreValue > 20) {
      // this.waterLevel = this.coimbatoreValue;
      this.areaName = this.areaName;
    //   $('#waterlevelPopUp').modal('show');
    // }  
    // if (this.maduraiValue > 11) {
    //   this.waterLevel = this.maduraiValue;
    //   this.areaName = this.areaName;
    //   $('#waterlevelPopUp').modal('show');
    // }  if (this.salemValue > 15) {
    //   this.waterLevel = this.salemValue;
    //   this.areaName = this.areaName;
    //   $('#waterlevelPopUp').modal('show');
    // }  if (this.thanjavurValue > 6) {
    //   this.waterLevel = this.thanjavurValue;
    //   this.areaName = this.areaName;
    //   $('#waterlevelPopUp').modal('show');
    // }
    //  if (this.tiruchirapalli > 12) {
    //   this.waterLevel = this.tiruchirapalli;
    //   this.areaName = this.areaName;
    //   $('#waterlevelPopUp').modal('show');
    // }  if (this.velloreValue > 12.5) {
    //   this.waterLevel = this.velloreValue;
    //   this.areaName = this.areaName;
    //   $('#waterlevelPopUp').modal('show');
    // }  if (this.tirunelveliValue > 13) {
    //   this.waterLevel = this.tirunelveliValue;
    //   this.areaName = this.areaName;
    //   $('#waterlevelPopUp').modal('show');
    // }  if (this.tiruppurValue > 18) {
    //   this.waterLevel = this.tiruppurValue;
    //   this.areaName = this.areaName;
    //   $('#waterlevelPopUp').modal('show');
    // } if (this.thoothkudiValue > 11) {
    //   this.waterLevel = this.thoothkudiValue;
    //   this.areaName = this.areaName;
    //   $('#waterlevelPopUp').modal('show');
    // } if (this.erode > 16) {
    //   this.waterLevel = this.erode;
    //   this.areaName = this.areaName;
    //   $('#waterlevelPopUp').modal('show');
    // }
  }
  pressureChanged(x) {
  //  console.log('xxxxxxxx00', x); 
  //  console.log(document.getElementById('pressureChanged'));
// alert(document.getElementById('pressureChanged'))
  
  if (x === 'Thanjavur') {
    this.textAlert = 'Water Levels Increased In ' + x
  } else if (x === 'Tirunelveli') {
   this.textAlert = 'Water Levels Increased In ' + x
  } else if (x === 'Thoothukudi') {
   this.textAlert = 'Water Levels Increased In ' + x
  } else if (x === 'Coimbatore') { 
    this.textAlert = 'Major Fire Breakes Out In Ramnathapuram.'
  } else {
   this.textAlert = 'Major Fire Breakes Out In ' + x
  }
    if (x === 'Coimbatore') {
      // $('#waterlevelPopUp').modal('show');
      this.i = x;
      this.areaName = x;
      // if(this.coimbatoreValue > 20) {
      //   this.waterLevel = this.coimbatoreValue;
      //   this.areaName = x;
      //   $('#waterlevelPopUp').modal('show');
      // }
      this.markers = [
                {
                  lat: 11.0168,
                  lng: 76.9558,
                  label: 'C',
                  draggable: true
                },
             ];
      this.lat = 11.0168;
      this.lng = 76.9558;
      console.log(this.lat, this.lng, 'latttttttttttttttttttttttttt');
      this.city = 'Coimbatore';
      this.yesterdayWind = '6kmph';
      this.todayWind = '7kmph';
      this.tomorrowWind = '5kmph';
      this.future1 = '8kmph';
      this.future2 = '6kmph';
      // weather
      this.yesterdayWeather = '6kmph';
      this.todayWeather = '7kmph';
      this.tomorrowWeather = '5kmph';
      this.futureWeather1 = '8kmph';
      this.futureWeather2 = '6kmph';
      // water
      this.futureWater1 = '2900Units';
    this.futureWater2 = '2530Units';
    this.tomorrowWater = '2520Units';
    this.todayWater = '2505Units';
    this.yesterdayWater = '2300Units';
    // news
    this.news1 = 'Coimbatore Corporation is all set to develop four parks under the Smart Cities mission.';
    this.news2 = 'Elephants rejuvenation camp in Mettupalayam.';
    this.news3 = 'Petrol Bomb Hurled at CPI(M) Office Coimbatore, No One Injured.';
    this.news4 = 'A record sighting of Frosted Duskywing butterfly.';
    this.news5 = 'Petrol Bombs Hurled at BJP Office in Coimbatore Hours After Vandals Damaged Periyar Statue.';
    this.news6 = 'Protests continue over Cauvery Board in Coimbatore.';
    this.news7 = 'BJP Office Attack: 3 TPDK Members Detained Under Goondas Act.';
    this.news8 = '4 Dead, Many Trapped as Bus Stand Roof Collapses in Coimbatore.';
    } else if (x === 'Madurai') {
      this.lat = 9.9252;
      this.lng = 78.1198;
    //   if (this.maduraiValue > 11) {
    //   this.waterLevel = this.maduraiValue;
      this.areaName = 'Madurai';
    //   $('#waterlevelPopUp').modal('show');
    // }
      this.markers = [
                {
                  lat: 9.9252,
                  lng: 78.1198,
                  label: 'M',
                  draggable: true
                },
              ];
      this.city = 'Madurai';
      this.yesterdayWind = '7kmph';
      this.todayWind = '9kmph';
      this.tomorrowWind = '11kmph';
      this.future1 = '6kmph';
      this.future2 = '4kmph';
      // weather
      this.yesterdayWeather = '7kmph';
      this.todayWeather = '9kmph';
      this.tomorrowWeather = '11kmph';
      this.futureWeather1 = '6kmph';
      this.futureWeather2 = '4kmph';
      // water
      this.futureWater1 = '2300Units';
    this.futureWater2 = '2600Units';
    this.tomorrowWater = '2500Units';
    this.todayWater = '1900Units';
    this.yesterdayWater = '2590Units';
    // news data
   
    this.news1 = 'Dhinakaran Launches Own "Amma" Party, Mocked By AIADMK As "Mosquito".';
    console.log('in madurai', this.news1);
    this.news2 = 'Policeman On Duty Shoots Himself At Jayalalithaas Memorial.';
    this.news3 = 'Kamal Haasan Launches Political Party "Makkal Needhi Maiam".';
    this.news4 = 'Nirmala Sitharamans Journey: From Madurai to South Block via JNU.';
    this.news5 = 'Lankan Airline Crew Crawl to Enter Restricted Area at Madurai.';
    this.news6 = '2 Killed, 50 Injured in Jallikattu at Pudukottai; 1 Dead in Madurai Protests.';
    this.news7 = 'Madurai District Administration Ready to Hold Jallikattu at Alanganallur, Say Officials.';
    this.news8 = 'Jallikattu Supporters Continue Protests in Tamil Nadu, Many Detained.';
    // console.log('in madura--i', this.news1);
    } else if (x === 'Salem') {
      this.lat = 11.6643;
      this.lng = 78.1460;
      // if (this.salemValue > 15) {
      //     this.waterLevel = this.salemValue;
          this.areaName = 'Salem';
      //     $('#waterlevelPopUp').modal('show');
      //   }
      this.markers = [
                {
                  lat: 11.6643,
                  lng: 78.1460,
                  label: 'S',
                  draggable: true
                },
              ];
      this.city = 'Salem';
      this.yesterdayWind = '8kmph';
      this.todayWind = '11kmph';
      this.tomorrowWind = '10kmph';
      this.future1 = '5kmph';
      this.future2 = '9kmph';
      // weather
      this.yesterdayWeather = '8kmph';
      this.todayWeather = '11kmph';
      this.tomorrowWeather = '10kmph';
      this.futureWeather1 = '5kmph';
      this.futureWeather2 = '9kmph';
      // water
      this.futureWater1 = '2300Units';
    this.futureWater2 = '2500Units';
    this.tomorrowWater = '2000Units';
    this.todayWater = '2900Units';
    this.yesterdayWater = '2800Units';
        // news
    this.news1 = 'Ola driver sets wife ablaze inside car in Chennai, two children manage to escape.';
    this.news2 = 'Row over temple rituals: Five-day annual Nagapattinam festival suspended in TN.';
    this.news3 = 'Ensure quota for Dalits in private sector jobs: DMK chief Karunanidhi tells Centre.';
    this.news4 = 'Four people dead, 2 wounded at an explosion in Salem.';
    this.news5 = 'AIADMK cadres stage hunger strike for CMB in Salem.';
    this.news6 = 'Groundwater level depletes in Salem region due to poor rainfall.';
    this.news7 = 'Eight trains run late as overhead cable snaps.';
    this.news8 = 'NMR to introduce summer special train services from tomorrow.';
    } else if (x === 'Thanjavur') {
      this.areaName = x;
      this.lat = 10.7870;
      this.lng = 79.1378;
      this.markers = [
                {
                  lat: 10.7870,
                  lng: 79.1378,
                  label: 'T',
                  draggable: true
                },
              ];
      this.city = 'Thanjavur';
      this.yesterdayWind = '8kmph';
      this.todayWind = '8kmph';
      this.tomorrowWind = '11kmph';
      this.future1 = '6kmph';
      this.future2 = '5kmph';
      // weather
      this.yesterdayWeather = '8kmph';
      this.todayWeather = '8kmph';
      this.tomorrowWeather = '11kmph';
      this.futureWeather1 = '6kmph';
      this.futureWeather2 = '5kmph';
      // water
      this.futureWater1 = '2590Units';
    this.futureWater2 = '2300Units';
    this.tomorrowWater = '2800Units';
    this.todayWater = '2100Units';
    this.yesterdayWater = '2900Units';
    // news
     this.news1 = 'Toll goes up to 10 in bus mishap in Tamil Nadus Thanjavur.';
     this.news2 = 'Thanjavur: Car festival of Brahadheeswara temple held.';
     this.news3 = 'Tamil Nadu fishermen to "lay siege" to Sri Lanka Deputy High Commission on Feb 29.';
     this.news4 = 'Toll goes up to 10 in bus mishap in Tamil Nadus Thanjavur.';
     this.news5 = 'EC Defers Poll to Thanjavur Constituency Over Money Power.';
     this.news6 = 'High drama at Uzhavan Express inauguration leaves DMK worker dead.';
     this.news7 = 'Cauvery: A River That Isnt and The Backlash Thats Waiting.';
     this.news8 = 'Thanjavur Assembly Bypoll: AIADMK Leads With 5,993 Votes.';
    } else if (x === 'Tiruchirapalli') {
      this.areaName = x;
      this.city = 'Tiruchirapalli';
      this.lat= 10.7905;
      this.lng = 78.7047;
      this.markers = [
                {
                  lat: 10.7905,
                  lng: 78.7047,
                  label: 'T',
                  draggable: true
                },
              ];
      this.yesterdayWind = '7kmph';
      this.todayWind = '9kmph';
      this.tomorrowWind = '8kmph';
      this.future1 = '8kmph';
      this.future2 = '6kmph';
      // weather
      this.yesterdayWeather = '7kmph';
      this.todayWeather = '9kmph';
      this.tomorrowWeather = '8kmph';
      this.futureWeather1 = '8kmph';
      this.futureWeather2 = '6kmph';
      // water
      this.futureWater1 = '2500Units';
    this.futureWater2 = '2500Units';
    this.tomorrowWater = '2500Units';
    this.todayWater = '2500Units';
    this.yesterdayWater = '2500Units';
    // news
     this.news1 = 'Trichy lab contradicts NASA, says meteorite killed Vellore man.';
     this.news2 = 'BHEL Trichy Trade Apprentice 2017 Application Process Begins at bheltry.co.in; Last Date Oct 18th.';
     this.news3 = 'Coast Guard Dornier aircraft with 3 crew members onboard missing since Monday night.';
     this.news4 = 'Jonty Rhodes to Join TNPL Team Ruby Trichy Warriors.';
     this.news5 = '20 Sri Lankan refugees consume poison in Trichy refugee camp.';
     this.news6 = 'Modi praises "Hugging Amma" Mata Amritanandamayi in Malayalam.';
     this.news7 = 'Aishwarya Arjun an exception among star kids: Pandian.';
     this.news8 = 'Trichy feels poll heat as Jaya, Karuna visit.';
    } else if (x === 'Vellore') {
      this.lat= 12.9165;
      this.lng = 79.1325;
      this.areaName = x;
      this.markers = [
                {
                  lat: 12.9165,
                  lng: 79.1325,
                  label: 'V',
                  draggable: true
                },
              ];
      this.city = 'Vellore';
      this.yesterdayWind = '13kmph';
      this.todayWind = '15kmph';
      this.tomorrowWind = '14kmph';
      this.future1 = '13kmph';
      this.future2 = '15kmph';
      // weather
      this.yesterdayWeather = '13kmph';
      this.todayWeather = '15kmph';
      this.tomorrowWeather = '14kmph';
      this.futureWeather1 = '13kmph';
      this.futureWeather2 = '15kmph';
      // water
      this.futureWater1 = '2590Units';
    this.futureWater2 = '2520Units';
    this.tomorrowWater = '2504Units';
    this.todayWater = '2300Units';
    this.yesterdayWater = '2550Units';
    // news
    this.news1 = 'Student housing platform CoLive to expand to key university towns and cities in south India.';
    this.news2 = 'United Nations terror list has 139 Pakistan entries.';
    this.news3 = '97 non-venomous snake hatchlings rescued from house.';
    this.news4 = 'Vellores Christian Medical College Is Leaving 99 Of Its 100 MBBS Seats Vacant This Year.';
    this.news5 = 'VITEEE 2018 Online Application Process Begins for Vellore Institute of Technology, Apply Before 15th Mar 2018.';
    this.news6 = 'Vellore Engineering Students Ready to Test Their Formula Car in Germany.';
    this.news7 = 'Meteorite caused "mishap" in Vellore college which killed 1 person, says Jayalalithaa.';
    this.news8 = 'Watch: Last respects paid to Siachen martyr Havildar Elumalai in Vellore.';
    } else if (x === 'Tirunelveli') {
      this.city = 'Tirunelveli';
      this.lat = 8.7139;
      this.lng = 77.7567;
      this.areaName = x;
      this.markers = [
                {
                  lat: 8.7139,
                  lng: 77.7567,
                  label: 'T',
                  draggable: true
                },
              ];
      this.yesterdayWind = '3kmph';
      this.todayWind = '4kmph';
      this.tomorrowWind = '4kmph';
      this.future1 = '5kmph';
      this.future2 = '4kmph';
      // weather
      this.yesterdayWeather = '3kmph';
      this.todayWeather = '4kmph';
      this.tomorrowWeather = '4kmph';
      this.futureWeather1 = '5kmph';
      this.futureWeather2 = '4kmph';
      // water
      this.futureWater1 = '2500Units';
    this.futureWater2 = '2500Units';
    this.tomorrowWater = '2500Units';
    this.todayWater = '2500Units';
    this.yesterdayWater = '2500Units';
       // news
    this.news1 = 'Tamil Nadu: Woman hacked to death at childrens home in Tirunelveli.';
    this.news2 = 'Rowdy killed in "encounter" in Tirunelveli.';
    this.news3 = 'Widespread rain in Tirunelveli, Kanyakumari districts.';
    this.news4 = 'Indian ISIS operative says he met Paris bombing accused in Iraq last year.';
    this.news5 = 'TN assembly election 2016: Polling begins on a dull note in southern districts.';
    this.news6 = 'Madras HC helps Class X student to appear for board exam.';
    this.news7 = 'Flood-hit Tuticorin limps back to normalcy.';
   this.news8 = 'Watch: When Subramanian Swamy shocked people at a wedding!.';
    } else if (x === 'Tiruppur') {
      this.city = 'Tiruppur';
      this.lat = 11.1085;
      this.lng = 77.3411;
      this.areaName = x;
      this.markers = [
                {
                  lat: 11.1085,
                  lng: 77.3411,
                  label: 'T',
                  draggable: true
               },
             ];
      this.yesterdayWind = '7kmph';
      this.todayWind = '8kmph';
      this.tomorrowWind = '6kmph';
      this.future1 = '7kmph';
      this.future2 = '6kmph';
      // weather
      this.yesterdayWeather = '7kmph';
      this.todayWeather = '8kmph';
      this.tomorrowWeather = '6kmph';
      this.futureWeather1 = '7kmph';
      this.futureWeather2 = '6kmph';
      // water
      this.futureWater1 = '2550Units';
    this.futureWater2 = '2520Units';
    this.tomorrowWater = '2550Units';
    this.todayWater = '2000Units';
    this.yesterdayWater = '2500Units';
    // news
    this.news1 = 'Policeman Slaps Woman; Tamil Nadu Govt Promises to Take Action.';
    this.news2 = 'Tamil Nadu: One held in Tirupur for wearing T-shirts praising ISIS.';
    this.news3 = 'Budget yatra: Tirupur, the knitwear capital of India, loses its sheen.';
    this.news4 = 'Swine flu strikes again in Tirupur.';
    this.news5 = 'Brisk polling in Coimbatore and Tirupur dists.';
    this.news6 = '5 Students Suspended for Coming to School After Booze.';
    this.news7 = 'LS polls: DMK moves HC seeking repoll in 9 booths in Tamil Nadu.';
    this.news8 = 'NHRC issues notice to TN government over killing of Dalit man.';
    } else if (x === 'Thoothukudi') {
      this.city = 'Thoothukudi';
      this.lat = 8.7642;
      this.lng = 78.1348;
      this.areaName = x;
      this.markers = [
                {
                  lat: 8.7642,
                  lng: 78.1348,
                  label: 'T',
                  draggable: true
                },
              ];
      this.yesterdayWind = '4kmph';
      this.todayWind = '3kmph';
      this.tomorrowWind = '2kmph';
      this.future1 = '4kmph';
      this.future2 = '3kmph';
      // weather
      this.yesterdayWeather = '4kmph';
      this.todayWeather = '3kmph';
      this.tomorrowWeather = '2kmph';
      this.futureWeather1 = '4kmph';
      this.futureWeather2 = '3kmph';
      // water
      this.futureWater1 = '2500Units';
    this.futureWater2 = '2500Units';
    this.tomorrowWater = '2500Units';
    this.todayWater = '2500Units';
    this.yesterdayWater = '2500Units';
    // news
    this.news1 = 'Tamil Nadu fishermen to "lay siege" to Sri Lanka Deputy High Commission on Feb 29.';
    this.news2 = 'Indian Coast Guard apprehends Sri Lankan boat, 7 crew members for illegal fishing.';
    this.news3 = 'BharatBenz Widens Reach in Tamil Nadu With Tuticorin Dealership.';
    this.news4 = 'Lankan Navy arrests 28 fishermen on charges of crossing International Maritime Boundary Line.';
    this.news5 = '33 crew members of US ship MV Seaman questioned by Tuticorin police.';
    this.news6 = '23 foreign nationals, 12 Indians sent to jail for 5 years in 2013 US ship detention case.';
    this.news7 = 'Tamil Nadu: Boat capsizes in Tuticorin, nine dead.';
    this.news8 = 'New airline Air Pegasus to start flight operations from April 12.';
    } else if (x === 'Erode') {
      this.lat = 11.3410;
      this.lng = 77.7172;
      this.areaName = x;
      this.markers = [
                {
                  lat: 11.3410,
                  lng: 77.7172,
                  label: 'T',
                  draggable: true
                },
              ];
      this.city = 'Erode';
      this.yesterdayWind = '7kmph';
      this.todayWind = '6kmph';
      this.tomorrowWind = '7kmph';
      this.future1 = '5kmph';
      this.future2 = '7kmph';
      // weather
      this.yesterdayWeather = '7kmph';
      this.todayWeather = '6kmph';
      this.tomorrowWeather = '7kmph';
      this.futureWeather1 = '5kmph';
      this.futureWeather2 = '7kmph';
      // water
      this.futureWater1 = '2500Units';
    this.futureWater2 = '2500Units';
    this.tomorrowWater = '2500Units';
    this.todayWater = '2500Units';
    this.yesterdayWater = '2500Units';
    // news
    this.news1 = 'Statue Politics: Periyar Isnt Lenin and Tirupattur Isnt Tripura.';
    this.news2 = 'Tamil Nadu: 7 workers die after inhaling poisonous gas in Erode.';
    this.news3 = 'BharatBenz Widens Reach in Tamil Nadu With Tuticorin Dealership.';
    this.news4 = 'Lankan Navy arrests 28 fishermen on charges of crossing International Maritime Boundary Line.';
    this.news5 = '33 crew members of US ship MV Seaman questioned by Tuticorin police.';
    this.news6 = '23 foreign nationals, 12 Indians sent to jail for 5 years in 2013 US ship detention case.';
    this.news7 = 'Tamil Nadu: Boat capsizes in Tuticorin, nine dead.';
   this.news8 = 'New airline Air Pegasus to start flight operations from April 12.';
    }
  }
  clickedMarker(label: string, index: number) {
    // console.log(`clicked the marker: ${label || index}`);
  }
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  digitalAsset() {
    this.router.navigate(['digitalAsset/'+ this.selectedValue]);

  }
  incident() {
    // console.log('in incident navigation page');
    // this.obj = {
    //   'subject' : 'Flood',
    //   'area': 'Erode',
    //   'issue' : 'Flood is increased in Erode Area'
    // }
    // this.GS.trueDeskPost(this.obj).subscribe(data => {
    //   console.log('floodddd ', data);
    // });
    // this.router.navigate(['incident/' + this.city]);
    this.router.navigate(['incidents/' + this.city]);
    $('#waterlevelPopUp').modal('hide');
    
   
  }
  incidentDash() {
    // alert(document.getElementById('incident').id);
    document.getElementById('incident').id
  }
  environmental() {
  //  console.log('city nameeeeeeee ', this.areaName);
    this.router.navigate(['weather/' + this.areaName]);
    this.paramData = this.route.params.subscribe(params => {
      this.areaName = params.id;
      this.city = params.id;
      this.selectedValue = this.city;
      if (this.city === 'Thanjavur') {
        this.textAlert = 'Water Levels Increased In ' + this.city
      } else if (this.city === 'Tirunelveli') {
       this.textAlert = 'Water Levels Increased In ' + this.city
      } else if (this.city === 'Thoothukudi') {
       this.textAlert = 'Water Levels Increased In ' + this.city
      } else if (this.city === 'Coimbatore') { 
       this.textAlert = 'Major Fire Breakes Out In Ramnathapuram.'
     } else {
       this.textAlert = 'Major Fire Breakes Out In ' + this.city
      }
      this.pressureChanged(this.city);
      });
     
   this.maxValue = Math.floor(Math.random() * 20) + 1;
   // this.areaName = 'Coimbatore';
   $('#waterlevelPopUp').modal('hide');
   this.currentDate = new Date();
   
   this.future1date = this.currentDate.getDate() + 2;
   this.future2date = this.currentDate.getDate() + 3;
   // air pressure
  
   this.cityArray = ['Coimbatore', 'Madurai', 'Salem', 'Thanjavur', 'Tiruchirapalli', 'Vellore', 'Tirunelveli', 'Tiruppur', 'Thoothukudi', 'Erode']
 
   // water
   this.randomNumber = Math.floor(Math.random() * (2.5 - 1)) + 1;
   this.coimbatoreValue = 18.02 + this.randomNumber;
   this.maduraiValue = 9.41 + this.randomNumber;
   this.salemValue = 13.94 + this.randomNumber;
   this.thanjavurValue = 4.19 + this.randomNumber;
   this.tiruchirapalli = 9.89 + this.randomNumber;
   this.velloreValue = 10.26 + this.randomNumber;
   this.tirunelveliValue = 10.69 + this.randomNumber;
   this.tiruppurValue = 15.16 + this.randomNumber;
   this.thoothkudiValue = 8.80 + this.randomNumber;
   this.erode = 13.17 + this.randomNumber;
   this.single = [
     {
       "name": "Coimbatore",
       "value": this.coimbatoreValue
     },
     {
       "name": "Madurai",
       "value": this.maduraiValue
     },
     {
       "name": "Salem",
       "value": this.salemValue
     },
     {
       "name": "Thanjavur",
       "value": this.thanjavurValue
     },
     {
       "name": "Tiruchirapalli",
       "value": this.tiruchirapalli
     },
     {
       "name": "Vellore",
       "value": this.velloreValue
     },
     {
       "name": "Tirunelveli",
       "value": this.tirunelveliValue
     },
     {
       "name": "Tiruppur",
       "value": this.tiruppurValue
     },
     {
       "name": "Thoothukudi",
       "value": this.thoothkudiValue
     },
     {
       "name": "Erode",
       "value": this.erode
     }
   ];
  }
  detail() {
    this.router.navigate(['natural/' + this.city]);
    
    $('#waterlevelPopUp').modal('hide');
  }
  markers: marker[] = [
    {
		  lat: 11.0168,
		  lng: 76.9558,
		  label: 'C',
		  draggable: true
	  },
  ];
  
}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}


$(document).ready(function () {
  //Toggle fullscreen
  $("#panel-fullscreen").click(function (e) {
      e.preventDefault();
      
      var $this = $(this);
  
      if ($this.children('i').hasClass('glyphicon-resize-full'))
      {
          $this.children('i').removeClass('glyphicon-resize-full');
          $this.children('i').addClass('glyphicon-resize-small');
      }
      else if ($this.children('i').hasClass('glyphicon-resize-small'))
      {
          $this.children('i').removeClass('glyphicon-resize-small');
          $this.children('i').addClass('glyphicon-resize-full');
      }
      $(this).closest('.panel').toggleClass('panel-fullscreen');
  });
  $("#panel-fullscreen1").click(function (e) {
    e.preventDefault();
    
    var $this = $(this);

    if ($this.children('i').hasClass('glyphicon-resize-full'))
    {
        $this.children('i').removeClass('glyphicon-resize-full');
        $this.children('i').addClass('glyphicon-resize-small');
    }
    else if ($this.children('i').hasClass('glyphicon-resize-small'))
    {
        $this.children('i').removeClass('glyphicon-resize-small');
        $this.children('i').addClass('glyphicon-resize-full');
    }
    $(this).closest('.panel').toggleClass('panel-fullscreen');
});
});
