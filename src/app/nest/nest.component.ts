import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nest',
  templateUrl: './nest.component.html',
  styleUrls: ['./nest.component.css']
})
export class NestComponent implements OnInit {
  totalPath : any;
  batteryHealth: any;
  smokeTest: any;
  coState: any;
  afzal: any;
  constructor() { }

  ngOnInit() {
    // this.listerner1();
    let eventSource = window['EventSource'];
    // console.log('eentttttttttttttt ', eventSource);
    let source = new eventSource('https://developer-api.nest.com' + '?auth=' + 'c.XMyCgbDrcOlb0Go9kn7TdC7KSdovJ33cak2m0peNlx6q1KXvQoa58u9JAPBjDlHyXBuXqVGSbC2gLj9aqpIN8EZhtqoj7xzuO4GCSRc4Xcp13oefka35WfbnXztFxLqqboT7kkM45VgerYqd');
    // console.log('sourceeeeeeeeeeeeeeeeeeee ', source);
    source.addEventListener('put', function(e) {
      // console.log('putttttttttttttt ', JSON.parse(e.data).data.devices.smoke_co_alarms['Wct2ztIDRfDOyNlmmprffOxgySsCgM0k']);
      this.totalPath = JSON.parse(e.data).data.devices.smoke_co_alarms['Wct2ztIDRfDOyNlmmprffOxgySsCgM0k'];
      this.batteryHealth = this.totalPath['battery_health']
      this.smokeTest = this.totalPath['smoke_alarm_state']
      this.coState = this.totalPath['co_alarm_state']
      console.log('battery health ', this.batteryHealth, this.smokeTest, this.coState);
      // this.afzal = 'Afzal'
    });
  }
  
  listerner1() {
    let eventSource = window['EventSource'];
    // console.log('eentttttttttttttt ', eventSource);
    let source = new eventSource('https://developer-api.nest.com' + '?auth=' + 'c.XMyCgbDrcOlb0Go9kn7TdC7KSdovJ33cak2m0peNlx6q1KXvQoa58u9JAPBjDlHyXBuXqVGSbC2gLj9aqpIN8EZhtqoj7xzuO4GCSRc4Xcp13oefka35WfbnXztFxLqqboT7kkM45VgerYqd');
    // console.log('sourceeeeeeeeeeeeeeeeeeee ', source);
    source.addEventListener('put', function(e) {
      // console.log('putttttttttttttt ', JSON.parse(e.data).data.devices.smoke_co_alarms['Wct2ztIDRfDOyNlmmprffOxgySsCgM0k']);
      this.totalPath = JSON.parse(e.data).data.devices.smoke_co_alarms['Wct2ztIDRfDOyNlmmprffOxgySsCgM0k'];
      this.batteryHealth = this.totalPath['battery_health']
      this.smokeTest = this.totalPath['smoke_alarm_state']
      this.coState = this.totalPath['co_alarm_state']
      console.log('battery health ', this.batteryHealth, this.smokeTest, this.coState);
      this.afzal = 'Afzal'
    });
  }
}
