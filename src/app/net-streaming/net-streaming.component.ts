import { Component, OnInit, ViewChild } from '@angular/core';
import { } from '@types/googlemaps';

declare var google: any;
@Component({
  selector: 'app-net-streaming',
  templateUrl: './net-streaming.component.html',
  styleUrls: ['./net-streaming.component.css']
})
export class NetStreamingComponent implements OnInit {
  @ViewChild('gmap') gmapElement: any;
  // map: google.maps.Map;
  directionsService: any;
  request: { origin: any; destination: any; travelMode: any; };
  constructor() { }

  ngOnInit() {
   
  
    let map = new google.maps.Map(document.getElementById("map"), {
      center: {lat: 11.0180549, lng: 76.9606132},
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    this.getDirections(map);
  }
  getDirections(map) {
    this.directionsService = new google.maps.DirectionsService();
    this.request = {
      origin: new google.maps.LatLng(11.0180549, 76.9606132),
      destination: new google.maps.LatLng(11.0168445, 76.9558322),
      travelMode: google.maps.TravelMode.DRIVING
      };
      this.directionsService.route(this.request, function(result, status) {
        console.log('statusssssssss ',  result.routes[0].overview_path[0]);
        if (status == google.maps.DirectionsStatus.OK) {
					this.autoRefresh(map, result.routes[0].overview_path);
				}
       
      });
  }
  autoRefresh(map, pathCoords) {
    let i, route, marker;
			
    route = new google.maps.Polyline({
      path: [],
      geodesic : true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2,
      editable: false,
      map:map
    });
    marker=new google.maps.Marker({map:map, icon:"http://maps.google.com/mapfiles/ms/micons/blue.png"});
			for (i = 0; i < pathCoords.length; i++) {				
				setTimeout(function(coords) {
					route.getPath().push(coords);
					this.moveMarker(map, marker, coords);
				}, 200 * i, pathCoords[i]);
			}
  }
  moveMarker(map, marker, latlng) {
    marker.setPosition(latlng);
    map.panTo(latlng);
  }
}
