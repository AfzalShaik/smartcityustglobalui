import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetStreamingComponent } from './net-streaming.component';

describe('NetStreamingComponent', () => {
  let component: NetStreamingComponent;
  let fixture: ComponentFixture<NetStreamingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetStreamingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetStreamingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
