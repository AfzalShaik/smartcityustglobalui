import { NgModule, OnInit, Component } from '@angular/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
declare var $: any;
// import { ToastrService } from 'ngx-toastr';
// import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-natural',
  templateUrl: './natural.component.html',
  styleUrls: ['./natural.component.css'],
  providers : [GraphsService]
})
export class NaturalComponent implements OnInit {

  nameCity1: any;
  nameCity: any;
  textAlert: any;
  title: string;
  hospital: string;
  distance: string;
  area: any;
  city: any;
  paramData: any;
  tempPoliceObj: { 'issue': string; 'subject': string; 'area': string; };
  waterHospitalObj: { 'issue': string; 'subject': string; 'area': string; };
  waterFireObj: { 'issue': string; 'subject': string; 'area': string; };
  waterPoliceObj: {};
  cityArray: string[];
  constructor(private GS: GraphsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.paramData = this.route.params.subscribe(params => {
      this.city = params.id;
      if (this.city == 'Salem') {
        this.area = 'Attur';
        this.distance = '600m';
        this.hospital = 'SUNDARAM MULTI SPECIALITY HOSPITAL';
        this.title = 'Major Fire Breaks out in ' + this.city;
      } else if (this.city == 'Coimbatore') {
        this.area = 'Ramnathapuram';
        this.distance = '650m';
        this.hospital = 'PSG HOSPITAL';
        this.title = 'Major Fire Breaks out in ' + this.city;
      } else if (this.city == 'Madurai') {
        this.area = 'Koodal Nagar';
        this.distance = '550m';
        this.hospital = 'VADAMALAYAN HOSPITAL';
        this.title = 'Major Fire Breaks out in ' + this.city;
      } else if (this.city == 'Thanjavur') {
        this.area = 'Ayyampettai';
        this.distance = '500m';
        this.hospital = 'ANU HOSPITAL';
        this.title = 'Water Levels Increased In ' + this.city;
      } else if (this.city == 'Tiruchirapalli') {
        this.area = 'Sundar Nagar';
        this.distance = '450m';
        this.hospital = 'TIRUCHY MEDICAL CENTER & HOSPITAL';
        this.title = 'Major Fire Breaks out in ' + this.city;
      } else if (this.city == 'Vellore') {
        this.area = 'Katpadi';
        this.distance = '400m';
        this.hospital = 'KATPADI HOSPITAL';
        this.title = 'Major Fire Breaks out in ' + this.city;
      } else if (this.city == 'Tirunelveli') {
        this.area = 'Aygudi';
        this.distance = '800m';
        this.hospital = 'GALAXY HOSPITAL';
        this.title = 'Water Levels Increased In ' + this.city;
      } else if (this.city == 'Tiruppur') {
        this.area = 'Palladam';
        this.distance = '850m';
        this.hospital = 'RAMS HOSPITAL';
        this.title = 'Major Fire Breaks out in ' + this.city;
      } else if (this.city == 'Thoothukudi') {
        this.area = 'Periyanayagapuram';
        this.distance = '700m';
        this.hospital = 'NALLATHAMBI HOSPITAL';
        this.title = 'Water Levels Increased In ' + this.city;
      } else if (this.city == 'Erode') {
        this.area = 'Kollampalayam';
        this.distance = '650m';
        this.hospital = 'LOTUS HOSPITAL';
        this.title = 'Major Fire Breaks out in ' + this.city;
      } else {
        console.log(params.id.split("@$"));
        this.area = 'Kollampalayam';
        this.distance = '650m';
        this.hospital = 'LOTUS HOSPITAL';
        this.title = params.id.split("@$")[0];
        this.nameCity = params.id.split("@$")[1];
      }
      });
    this.cityArray = ['Coimbatore', 'Madurai', 'Salem', 'Thanjavur', 'Tiruchirapalli', 'Vellore', 'Tirunelveli', 'Tiruppur', 'Thoothukudi', 'Erode']
  }
  
  dashBoard() {
    this.nameCity1 = (this.nameCity) ? this.nameCity : this.city;
    this.router.navigate(['incidents/' + this.nameCity1]);
  }
  waterPolice() {
    this.waterPoliceObj = {
      'issue' : 'Water Level is increased. Take Appropriate Action.',
      'subject' : 'Flood',
      'area' : 'Coimbatore'
    }
    this.GS.waterPoliceService(this.waterPoliceObj).subscribe(data => {
      this.textAlert = 'Notification has been Sent to ' + this.area + ' Police Station.'; 
      $('#waterlevelPopUp1').modal('show');
      // alert('Notification has been send to nearest Police Station.');
      // console.log('floodddd ', data);
    });
  }
  waterFire() {
    this.waterFireObj = {
      'issue' : 'Water Level is increased. Take Appropriate Action.',
      'subject' : 'Flood',
      'area' : 'Coimbatore'
    }
    this.GS.waterFireService(this.waterPoliceObj).subscribe(data => {
      this.textAlert = 'Notification has been Sent to ' + this.area + ' Fire Station.'; 
      $('#waterlevelPopUp1').modal('show');
      // alert('Notification has been send to nearest Fire Station.');
      // console.log('floodddd ', data);
    });
  }
  waterHospital() {
    this.waterHospitalObj = {
      'issue' : 'Water Level is increased. Take Appropriate Action.',
      'subject' : 'Flood',
      'area' : 'Coimbatore'
    }
    this.GS.waterHospitalervice(this.waterPoliceObj).subscribe(data => {
      this.textAlert = 'Notification has been Sent to ' + this.hospital; 
      $('#waterlevelPopUp1').modal('show');
      // alert('Notification has been send to nearest Hospitals.');
      // console.log('floodddd ', data);
    });
  }
  tempPolice() {
    this.tempPoliceObj = {
      'issue' : 'Temperature Level is increased. Take Appropriate Action.',
      'subject' : 'Temperature',
      'area' : 'Coimbatore'
    }
    this.GS.tempPoliceService(this.tempPoliceObj).subscribe(data => {
      // console.log('floodddd ', data);
    });
  }
}
